import React from 'react';
import { Link } from 'react-router-dom';

import Carousel from '../components/Carousel/Carousel';

const Home = () => {
	return (
		<div>
			<Carousel />
			<div className="container">
				<div className="jumbotron">
					<h1>Bienvenido a mi aplicacion!</h1>
					<p>
						<Link className="btn btn-primary btn-lg" to="login">
							Iniciar sesion
						</Link>
					</p>
				</div>
			</div>
		</div>
	);
};

export default Home;
