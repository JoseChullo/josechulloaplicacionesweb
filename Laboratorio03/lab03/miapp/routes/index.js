var express = require('express');
var app = express(),

/*var express = require('express'),
app = express(),*/
bodyParser = require('body-parser'),
producto = require('./models/prod');

app.use(bodyParser.json());

app.set('view engine','jade');
app.get('/',function(req,res){
    res.send('Hola mundo');
})
app.get('/login',function(req,res){
    res.render('login');
});
app.get('/table',function(req,res){
  res.render('table');
})
app.get('/producto',producto.show);
app.post('/producto',producto.create);
app.post('/producto/update',producto.update);
app.post('/producto/delete',producto.delete);

app.listen(9090,function(){
    console.log('Iniciando!');
})

/*
app.use(express.static('public'))


app.get('/',function(req,res){
  res.send('Hola mundo! en express :)')
})

app.post('/',function(req,res){
  res.send('Llamada POST misma url')
})

app.put('/user',function(req,res){
  res.send('Recibimos u PUT en /user')
})

app.delete('/user',function(req,res){
  res.send('Recibimos un DELETE en /user')
})
*/
module.exports = app;
app.use(function(req,res,next){
  res.status(404).send("Eso no existe")
})

app.use(function(err,req,res,next){
  console.log(err.stack)
  res.status(500).send('Algo salio mal')
})
