import React, { Component } from 'react';
import { Tab, Nav } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
	faUser,
	faEdit,
	faKey,
	faCameraRetro
} from '@fortawesome/free-solid-svg-icons';

import getAvatar from '../../../utils/avatar';

import './ProfileDetails.css';

class ProfileDetails extends Component {
	state = {
		username: '',
		email: ''
	};
	componentDidMount() {
		this.setState({
			userName: localStorage.getItem('userName'),
			email: localStorage.getItem('email')
		});
	}
	passwordHandler = e => {
		alert('Funcion por implementar!');
	};
	pictureHandler = e => {
		alert('Funcion por implementar!');
	};
	render() {
		const urlLorem = 'https://www.lipsum.com/feed/html';
		return (
			<Tab.Container id="profile-tabs" defaultActiveKey="inicio">
				<div className="row pt-4">
					<div className="col-md-3">
						<div className="profile-img">
							<img
								src={getAvatar(this.state.email)}
								className="rounded-circle user_img"
								alt=""
							/>
						</div>
					</div>
					<div className="col-md-7">
						<div className="profile-head">
							<h5>
								<FontAwesomeIcon icon={faUser} />
								{this.state.userName}
							</h5>
							<h6>Web Develoér y Diseñador</h6>
							<p className="profile-rating">
								RANKINGS: <span>8/10</span>
							</p>
							<Nav>
								<Nav.Item>
									<Nav.Link className="profile-tab" eventKey="inicio">
										Tab 1
									</Nav.Link>
								</Nav.Item>
								<Nav.Item>
									<Nav.Link className="profile-tab" eventKey="mensajes">
										Tab 2
									</Nav.Link>
								</Nav.Item>
							</Nav>
						</div>
					</div>
					<div className="col-md-2">
						<button
							className="btn btn-info btn-block"
							onClick={() => this.props.history.push('/profile/edit')}
						>
							<FontAwesomeIcon icon={faEdit} /> Editar perfil
						</button>
						<button
							className="btn btn-primary btn-block"
							onClick={this.pictureHandler}
						>
							<FontAwesomeIcon icon={faCameraRetro} /> Imagen
						</button>
						<button
							className="btn btn-primary btn-block"
							onClick={this.passwordHandler}
						>
							<FontAwesomeIcon icon={faKey} /> Contraseña
						</button>
					</div>
				</div>
				<div className="row pb-4">
					<div className="col-md-3">
						<ul className="list-group">
							<li className="list-group-item text-muted">
								Actividad <i className="fa fa-dashboard fa-1x" />
							</li>
							<li className="list-group-item text-right">
								<span className="pull-left">
									<strong>Shares</strong>
								</span>{' '}
								125
							</li>
							<li className="list-group-item text-right">
								<span className="pull-left">
									<strong>Likes</strong>
								</span>{' '}
								13
							</li>
							<li className="list-group-item text-right">
								<span className="pull-left">
									<strong>Posts</strong>
								</span>{' '}
								37
							</li>
						</ul>
					</div>
					<div className="col-md-7">
						<Tab.Content>
							<Tab.Pane className="profile-pane" eventKey="inicio">
								Para generar un texto Lorem Ipsum utilizaremos la Url:
								<a href={urlLorem} target="_blank" rel="noopener noreferrer">
									{urlLorem}
								</a>
							</Tab.Pane>
							<Tab.Pane className="profile-pane" eventKey="mensajes">
								Lorem ipsum es el texto que se usa habitualmente en diseño
								gráfico en demostraciones de tipografías o de borradores de
								diseño para probar el diseño visual antes de insertar el texto
								final. Aunque no posee actualmente fuentes para justificar sus
								hipótesis, el profesor de filología clásica Richard McClintock
								asegura que su uso se remonta a los impresores de comienzos del
								siglo XVI.1​ Su uso en algunos editores de texto muy conocidos
								en la actualidad ha dado al texto lorem ipsum nueva popularidad.
								El texto en sí no tiene sentido, aunque no es completamente
								aleatorio, sino que deriva de un texto de Cicerón en lengua
								latina, a cuyas palabras se les han eliminado sílabas o letras.
								El significado del texto no tiene importancia, ya que solo es
								una demostración o prueba, pero se inspira en la obra de Cicerón
								De finibus bonorum et malorum (Sobre los límites del bien y del
								mal) que comienza con:
							</Tab.Pane>
						</Tab.Content>
					</div>
				</div>
			</Tab.Container>
		);
	}
}
export default ProfileDetails;
